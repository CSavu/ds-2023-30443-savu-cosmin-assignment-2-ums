package com.ums.configs;

import com.ums.exceptions.InvalidPropertyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { HttpClientErrorException.class })
    protected ResponseEntity<Object> handleHttpClientErrorException(RuntimeException ex, WebRequest request) {
        String responseMessage = "User does not have necessary permissions!";
        return handleExceptionInternal(ex, responseMessage, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(value = { InvalidPropertyException.class })
    protected ResponseEntity<Object> handleInvalidIdException(RuntimeException ex, WebRequest request) {
        String responseMessage = ex.getMessage();
        return handleExceptionInternal(ex, responseMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = { NullPointerException.class })
    protected ResponseEntity<Object> handleNullPointerException(RuntimeException ex, WebRequest request) {
        String responseMessage = "Null property: " + ex.getMessage();
        return handleExceptionInternal(ex, responseMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders httpHeaders,
                                                                  HttpStatus httpStatus,
                                                                  WebRequest request) {
        String responseMessage = "Validation failed with error: " + ex.getMessage();
        return handleExceptionInternal(ex, responseMessage, httpHeaders, HttpStatus.BAD_REQUEST, request);
    }
}
