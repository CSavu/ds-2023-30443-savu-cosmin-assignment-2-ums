package com.ums.responses;

import com.ums.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AuthorizationResponse extends SimpleUserResponse {
    private boolean isTokenValid;

    public AuthorizationResponse(String username, Role role, Integer userId, boolean isTokenValid) {
        super(username, role, userId);
        this.isTokenValid = isTokenValid;
    }
}