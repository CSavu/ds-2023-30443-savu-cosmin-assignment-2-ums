package com.ums.controllers;

import com.ums.entities.User;
import com.ums.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/getAll")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<User> getAllUsers() {
        return userService.getUsers();
    }

    @PostMapping("/create")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @DeleteMapping("/delete")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Boolean deleteUser(@RequestParam Integer userId) {
        return userService.deleteUser(userId);
    }

    @PatchMapping("/update")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public User updateUser(@Valid @RequestBody User user) {
        return userService.updateUser(user);
    }

    @GetMapping("/getById")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public User getUserById(@RequestParam Integer userId) {
        return userService.getById(userId);
    }
}
