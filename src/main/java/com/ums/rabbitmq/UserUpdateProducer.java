package com.ums.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UserUpdateProducer {
    @Value("${ums.rabbitmq.user.exchange}")
    private String userExchange;
    @Value("${ums.rabbitmq.user.delete.routing_key}")
    private String userDeleteRoutingKey;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void publishUserDeletionMessage(String userId) {
        rabbitTemplate.convertAndSend(userExchange, userDeleteRoutingKey, userId);
        log.info("User deletion message sent for userId={}", userId);
    }
}
